/*
 * SuperDuper-App.cxx
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


#include <config-persistence.h>

using namespace cfg_persistence;

int main()
{
  std::map<std::string, std::string> appConfiguration;

  IConfigLocationPtr location = std::make_shared<ConfigLocation>("SuperDuper-App");
  IConfigIOPtr myConfig = std::make_shared<ConfigIO>(location);
  myConfig->create_config_directory();
  if (myConfig->config_exists())
    appConfiguration = myConfig->read_config_tag_value());

  // do
  // something
  // useful

  // Finally save the configuration
  myConfig->save_config_tag_value(appConfiguration);

  myConfig = nullptr
  location = nullptr;

  exit 0;
};
