/* SPDX-License-Identifier: Apache-2.0
 * ConfigLocation.windows.cxx
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "ConfigLocation.h"

#include <cstdlib>


namespace cfg_persistence {
  std::filesystem::path cfg_persistence::ConfigLocation::get_config_location() const
  {
    constexpr auto APPDATA = "APPDATA";
    std::filesystem::path configLocation;


    std::string appdata = this->get_env_var(APPDATA);

    if (appdata.empty())
      throw std::runtime_error("No suitable location found");

    configLocation = std::filesystem::path(appdata);
    if (!this->vendorName.empty())
      configLocation /= std::filesystem::path(this->vendorName);
    configLocation /= std::filesystem::path(this->appName);

    return configLocation;
  };
}
