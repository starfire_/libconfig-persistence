/* SPDX-License-Identifier: Apache-2.0
 * TestConfigIO.cxx
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "TestConfigIO.h"
#include "../ConfigLocation.h"

#include <filesystem>
#include <fstream>


namespace cfg_persistence {
  namespace test {
    void TestConfigIO::SetUp()
    {
      cfg_persistence::IConfigLocationPtr cfg_location_mock_ptr = std::shared_ptr<cfg_persistence::IConfigLocation>(&cfg_location_mock.get(), [](...) {});
      this->cut = std::make_shared<ConfigIO>(cfg_location_mock_ptr);
    }

    void TestConfigIO::TearDown()
    {
      this->cut = nullptr;
      this->cfg_location_mock_ptr = nullptr;
    }

    TEST_F(TestConfigIO, create_config_directory_test)
    {
      cfg_persistence::IConfigLocationPtr test_location;
      test_location = std::make_shared<cfg_persistence::ConfigLocation>("TestApp");

      this->cut = nullptr;
      this->cut = std::make_shared<ConfigIO>(test_location);

      if (std::filesystem::exists(test_location->get_config_location()))
        std::filesystem::remove_all(test_location->get_config_location());

      EXPECT_FALSE(std::filesystem::exists(test_location->get_config_location()));

      this->cut->create_config_directory();
      EXPECT_TRUE(std::filesystem::exists(test_location->get_config_location()));

      if (std::filesystem::exists(test_location->get_config_location()))
        std::filesystem::remove_all(test_location->get_config_location());

      test_location = nullptr;
    }

    TEST_F(TestConfigIO, read_config_tag_value_test)
    {
      std::filesystem::path inexisting_file_path("data/NotExist.cfg");
      inexisting_file_path.make_preferred();
      std::filesystem::path test_file_path("data/TestConfig.cfg");
      test_file_path.make_preferred();
      std::map<std::string, std::string> tag_value_data;

      fakeit::When(Method(cfg_location_mock, get_config_filename)).Return(inexisting_file_path.string());
      EXPECT_THROW(tag_value_data = this->cut->read_config_tag_value(), std::filesystem::filesystem_error);

      fakeit::When(Method(cfg_location_mock, get_config_filename)).Return(test_file_path.string());
      tag_value_data = this->cut->read_config_tag_value();

      EXPECT_EQ(3, tag_value_data.size());

      auto found = tag_value_data.find("animal");
      EXPECT_TRUE(found != tag_value_data.end());
      EXPECT_EQ("lion", found->second);

      found = tag_value_data.find("fruit");
      EXPECT_TRUE(found != tag_value_data.end());
      EXPECT_EQ("strawberry", found->second);

      found = tag_value_data.find("vegetable");
      EXPECT_TRUE(found != tag_value_data.end());
      EXPECT_EQ("potato", found->second);
    }

    TEST_F(TestConfigIO, save_config_tag_value_test)
    {
      std::map<std::string, std::string> save_data {
        { "animal", "cheetah" },
        { "fruit", "blueberry" },
        { "vegetable", "spinach" }
      };
      std::filesystem::path test_file_path("data/TestSave.cfg");
      test_file_path.make_preferred();
      std::filesystem::path ref_file_path("data/TestSaveReference.cfg");
      ref_file_path.make_preferred();

      fakeit::When(Method(cfg_location_mock, get_config_filename)).Return(test_file_path.string());

      this->cut->save_config_tag_value(save_data);

      EXPECT_TRUE(std::filesystem::exists(test_file_path));

      std::ifstream test_file(test_file_path.string());
      std::string test_content( (std::istreambuf_iterator<char>(test_file)),
                                (std::istreambuf_iterator<char>()) );
      test_file.close();
      std::ifstream reference_file(ref_file_path.string());
      std::string ref_content( (std::istreambuf_iterator<char>(reference_file)),
                               (std::istreambuf_iterator<char>()) );
      reference_file.close();
      EXPECT_EQ(ref_content, test_content);
    }

    TEST_F(TestConfigIO, read_config_xml_test)
    {
      std::filesystem::path test_file_path("data/TestConfig_xml.cfg");
      test_file_path.make_preferred();
      fakeit::When(Method(cfg_location_mock, get_config_filename)).Return(test_file_path.string());

      tinyxml2::XMLDocument *test_doc = this->cut->read_config_xml();

      EXPECT_TRUE(test_doc != nullptr);

      EXPECT_STREQ("TestConfiguration", test_doc->RootElement()->Name());
      EXPECT_FALSE(test_doc->RootElement()->NoChildren());

      tinyxml2::XMLElement *test_elem = test_doc->RootElement()->FirstChildElement();
      EXPECT_TRUE(test_elem != nullptr);
      EXPECT_STREQ("Configuration", test_elem->Name());
      EXPECT_STREQ("lion", test_elem->GetText());
      const tinyxml2::XMLAttribute *test_attr = test_elem->FindAttribute("type");
      EXPECT_TRUE(test_attr != nullptr);
      EXPECT_STREQ("animal", test_attr->Value());

      test_elem = test_elem->NextSiblingElement();
      EXPECT_TRUE(test_elem != nullptr);
      EXPECT_STREQ("Configuration", test_elem->Name());
      EXPECT_STREQ("strawberry", test_elem->GetText());
      test_attr = test_elem->FindAttribute("type");
      EXPECT_TRUE(test_attr != nullptr);
      EXPECT_STREQ("fruit", test_attr->Value());

      test_elem = test_elem->NextSiblingElement();
      EXPECT_TRUE(test_elem != nullptr);
      EXPECT_STREQ("Configuration", test_elem->Name());
      EXPECT_STREQ("potato", test_elem->GetText());
      test_attr = test_elem->FindAttribute("type");
      EXPECT_TRUE(test_attr != nullptr);
      EXPECT_STREQ("vegetable", test_attr->Value());

      test_elem = test_elem->NextSiblingElement();
      EXPECT_TRUE(test_elem == nullptr);
    }

    TEST_F(TestConfigIO, save_config_xml_test)
    {
      std::filesystem::path test_file_path("data/TestSave_xml.cfg");
      test_file_path.make_preferred();
      std::filesystem::path ref_file_path("data/TestSaveReference_xml.cfg");
      ref_file_path.make_preferred();

      tinyxml2::XMLDocument *test_doc = new tinyxml2::XMLDocument();
      tinyxml2::XMLDeclaration *test_decl = test_doc->NewDeclaration();
      tinyxml2::XMLNode *insert_pos = test_doc->InsertFirstChild(test_decl);
      tinyxml2::XMLElement *test_root_elem = test_doc->NewElement("MyConfig");
      test_root_elem->SetAttribute("version", "0.5");
      insert_pos = test_doc->InsertAfterChild(insert_pos, test_root_elem);
      tinyxml2::XMLElement *test_elem = test_doc->NewElement("EntryA");
      test_elem->SetText("Value of EntryA");
      insert_pos = test_root_elem->InsertFirstChild(test_elem);
      test_elem = test_doc->NewElement("EntryB");
      test_elem->SetAttribute("type", "entryBType");
      insert_pos = test_root_elem->InsertAfterChild(insert_pos, test_elem);

      fakeit::When(Method(cfg_location_mock, get_config_filename)).Return(test_file_path.string());
      this->cut->save_config_xml(test_doc);
      EXPECT_TRUE(std::filesystem::exists(test_file_path));

      std::ifstream test_file(test_file_path.string());
      std::string test_content( (std::istreambuf_iterator<char>(test_file)),
                                (std::istreambuf_iterator<char>()) );
      test_file.close();
      std::ifstream reference_file(ref_file_path.string());
      std::string ref_content( (std::istreambuf_iterator<char>(reference_file)),
                               (std::istreambuf_iterator<char>()) );
      reference_file.close();
      EXPECT_EQ(ref_content, test_content);
    }

  }
}
