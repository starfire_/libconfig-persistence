/* SPDX-License-Identifier: Apache-2.0
 * TestConfigIO.h
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 */


/** \file TestConfigIO.h
  * \brief Declaration for test class for ConfigIO
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2025-01-20
  */


#include <gtest/gtest.h>
#include <fakeit.hpp>
#include "../ConfigIO.h"


//! \namespace cfg_persistence
namespace cfg_persistence {
  //! \namespace test
  namespace test {
    /** \brief Test class for Config IO class
      *
      * Test class for the ConfgIO class. The tests in this class are
      * designed to work under Linux as well as under Windows.
      */
    class TestConfigIO : public ::testing::Test
    {
      protected:
        //! \brief Mock object for ConfigLocation
        fakeit::Mock<cfg_persistence::IConfigLocation> cfg_location_mock;
        //! \brief Shared pointer for mock ConfigLoation
        cfg_persistence::IConfigLocationPtr cfg_location_mock_ptr;
        cfg_persistence::IConfigIOPtr cut; //!< Class under test

        //! \brief Set the test class up
        void SetUp() override;
        //! \brief Clean the test class up after each test
        void TearDown() override;
    };

  }
}
