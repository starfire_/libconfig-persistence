/* SPDX-License-Identifier: Apache-2.0
 * Test_libconfig.cxx
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 */


#include <gtest/gtest.h>


int main(int argc, char **argv)
{
  testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
