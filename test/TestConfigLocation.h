/* SPDX-License-Identifier: Apache-2.0
 * TestConfigLocation.h
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 */


/** \file TestConfigLocation.h
  * \brief Declaration for test class for ConfigLocation
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2025-01-20
  */


#include <gtest/gtest.h>
#include "../IConfigLocation.h"
#include "../ConfigLocation.h"


//! \namespace cfg_persistence
namespace cfg_persistence {
  /** \namespace test
    * \brief Namespace for all tests
    *
    * This namespace encapsulates all unit tests to avoid name clashes
    */
  namespace test {

    /** \brief Test class for ConfigLocation
      *
      * Small test class for the ConfigLocation class. As this class has to
      * work unter Linux as well as under windows there are two different
      * implementation of this very same test class
      */
    class TestConfigLocation : public ::testing::Test
    {
      protected:
        cfg_persistence::IConfigLocationPtr cut;  //!< Class Under Test

        //! \brief Set the class under test up
        void SetUp() override;
        //! \brief Clean up after test
        void TearDown() override;

        /** \brief Replacement for C++20 "std::string::ends_with"
          *
          * This method checks if the end of the given full string matches
          * the end of the other given ending string
          *
          * \param fullString Full string for comparison
          * \param ending Partial string that should match the end
          *
          * \retval true The ending is at the end of the full string
          * \retval false The ending of the full strig does not match the
          *               ending string
          */
        bool str_ends_with(std::string const &fullString, std::string const &ending);
    };

  }
}
