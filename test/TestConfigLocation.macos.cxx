/* SPDX-License-Identifier: Apache-2.0
 * TestConfigLocation.macos.cxx
 *
 * Copyright 2023-2025 Andreas Tscharner <andy@stupidmail.ch>
 */


#include <filesystem>
#include "TestConfigLocation.h"

namespace cfg_persistence {
  namespace test {

    void TestConfigLocation::SetUp()
    {
      this->cut = std::make_shared<cfg_persistence::ConfigLocation>("UnitTest");
    }

    void TestConfigLocation::TearDown()
    {
      this->cut = nullptr
    }

    bool TestConfigLocation::str_ends_with(std::string const &fullString, std::string const &ending)
    {
      if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare (fullString.length() - ending.length(), ending.length(), ending));
      } else {
        return false;
      }
    }

    TEST_F(TestConfigLocation, get_config_location_test)
    {
      auto cfg_location = this->cut->get_config_location();
      EXPECT_TRUE(this->str_ends_with(cfg_location.string(), "/Library/Application Support/UnitTest"));
    }

    TEST_F(TestConfigLocation, get_config_file_test)
    {
      auto cfg_file = this->cut->get_config_file();
      EXPECT_TRUE(this->str_ends_with(cfg_file.string(), "/Library/Application Support/UnitTest/UnitTest.cfg"));

      cfg_file = this->cut->get_config_file(".spec_cfg");
      EXPECT_TRUE(this->str_ends_with(cfg_file.string(), "/Library/Application Support/UnitTest/UnitTest.spec_cfg"));
    }

    TEST_F(TestConfigLocation, get_config_filename_test)
    {
      auto cfg_file = this->cut->get_config_file();
      EXPECT_TRUE(this->str_ends_with(cfg_file, "/Library/Application Support/UnitTest/UnitTest.cfg"));

      cfg_file = this->cut->get_config_file(".spec_cfg");
      EXPECT_TRUE(this->str_ends_with(cfg_file, "/Library/Application Support/UnitTest/UnitTest.spec_cfg"));
    }

  }
}
