/* SPDX-License-Identifier: Apache-2.0
 * ConfigLocation.posix.cxx
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "ConfigLocation.h"

#include <cstdlib>


namespace cfg_persistence {
  std::filesystem::path ConfigLocation::get_config_location() const
  {
    constexpr auto XDG_CONFIG_HOME = "XDG_CONFIG_HOME";
    constexpr auto USER_HOME = "HOME";


    std::string xdg_config_home = this->get_env_var(XDG_CONFIG_HOME);
    std::string user_home = this->get_env_var(USER_HOME);

    if (!xdg_config_home.empty())
      return std::filesystem::path(xdg_config_home) / std::filesystem::path(this->appName);
    if (user_home.empty())
      throw std::runtime_error("No suitable location found");

    return std::filesystem::path(user_home) / std::filesystem::path(".config") / std::filesystem::path(this->appName);
  };
}
