/*
 * config-persistence.h
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file config-persistence.h
  * \brief Main header file for library
  *
  * This is the main header file for the config-persistence library. It
  * includes the header files of all classes and should be the only file to
  * include for the use of libconfig-persistence
  *
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2025-01-20
  */


#pragma once

#include "IConfigLocation.h"
#include "IConfigIO.h"
#include "ConfigLocation.h"
#include "ConfigIO.h"
