/* SPDX-License-Identifier: Apache-2.0
 * ConfigIO.cxx
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "ConfigIO.h"

#include <filesystem>
#include <fstream>
#include <sstream>
#include <algorithm>

namespace cfg_persistence {

  ConfigIO::ConfigIO(IConfigLocationPtr app_location)
  {
    this->app_location = app_location;
    this->config_file_extension = ".cfg";
  }

  void ConfigIO::set_config_file_extension(std::string file_ext)
  {
    this->config_file_extension = file_ext;
  }

  bool ConfigIO::config_exists()
  {
    std::filesystem::path cfg_file = this->app_location->get_config_file(this->config_file_extension);
    return std::filesystem::exists(cfg_file);
  }

  void ConfigIO::create_config_directory()
  {
    std::filesystem::create_directories(this->app_location->get_config_location());
  }

  std::map<std::string, std::string> ConfigIO::read_config_tag_value()
  {
    std::map<std::string, std::string> key_value_pairs;
    std::string key;
    std::string val;

    std::error_code err_code = std::make_error_code(std::io_errc::stream);
    std::string cfg_filename = this->app_location->get_config_filename(this->config_file_extension);
    std::filesystem::path cfg_filepath(cfg_filename);
    if (!std::filesystem::exists(cfg_filepath))
      throw std::filesystem::filesystem_error("Config file does not exist", cfg_filepath, err_code);

    std::ifstream cfg_file(cfg_filename);
    if (! cfg_file)
      throw std::filesystem::filesystem_error("Could not open config file", cfg_filepath, err_code);
    std::stringstream file_content;
    file_content << cfg_file.rdbuf();
    cfg_file.close();

    while(std::getline(std::getline(file_content, key, '=') >> std::ws, val)) {
      key.erase(std::remove(key.begin(), key.end(), ' '), key.end()); // trim(key)
      val.erase(std::remove(val.begin(), val.end(), ' '), val.end()); // trim(val)
      key_value_pairs.insert_or_assign(key, val);
    }

    return key_value_pairs;
  }

  void ConfigIO::save_config_tag_value(std::map<std::string, std::string> tag_value_data)
  {
    std::ofstream cfg_file(this->app_location->get_config_filename(this->config_file_extension));
    for(const auto &[key, val] : tag_value_data)
      cfg_file << key << "=" << val << std::endl;
    cfg_file.close();
  }

  tinyxml2::XMLDocument *ConfigIO::read_config_xml()
  {
    tinyxml2::XMLDocument *read_doc = new tinyxml2::XMLDocument();

    read_doc->LoadFile(this->app_location->get_config_filename(this->config_file_extension).c_str());
    return read_doc;
  }

  void ConfigIO::save_config_xml(tinyxml2::XMLDocument *xml_config_data)
  {
    xml_config_data->SaveFile(this->app_location->get_config_filename(this->config_file_extension).c_str());
  }

}
