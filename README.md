# libconfig-persistence

A small project to save configuration data of an application to the correct place. The library provides reading/parsing and writing for the simple "key = value" and the XML format.

# Build & Install from source

`cmake` is used to build `libconfic-persistence`.

Create a `_build` directory in the `libconfic-persistence` folder after you have cloned it. Change to it and run the following commands:

## Linux
    cmake -G Ninja -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<preferred installation dir> ..
    ninja install

    cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<preferred installation dir> ..
    make install

depending whether you want to use `ninja` or `Makefile`s. Leave the `install` option in the second line, if you only wnat to build.

## Windows
    cmake -G "Visual Studio 17 2022" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=<preferred installation dir> ..
    cmake --build . --config Release --target INSTALL

Use `ALL_BUILD` as target instead of `INSTALL' if you only want to build.


## Dependencies:

Make sure, `cmake` finds them

* [GoogleTest](https://github.com/google/googletest) for the unit tests
* [TinyXML](https://github.com/leethomason/tinyxml2) for the XML format (if available, otherwise it will be built)

## Used if available:

* [doxygen](https://www.doxygen.nl) for documentation in HTML and latex
* [pdflatex](https://tug.org/texlive/) if you want to create a PDF from the latex output


# Installation packages
Check the [GitLab Project Release Section](https://gitlab.com/starfire_/libconfig-persistence/-/releases) if there are any binary packages.