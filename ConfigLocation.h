/*
 * ConfigLocation.h
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file ConfigLocation.h
  * \brief Declaration of the ConfigLocation class
  *
  * This dervied class implements only the different methods
  *
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2023-03-12
  */

#pragma once

#include "ConfigLocationBase.h"

#include <filesystem>

//! \namespace cfg_persistence
namespace cfg_persistence {
  /** \brief Class that handles the different directories and files
    *
    * This class creates directory and file paths where the config should
    * be stored according to the operation system standards. It handles all
    * methods that are different in the supported operating systems, namely
    * Linux and Windows
    */
  class ConfigLocation : public ConfigLocationBase
  {
    public:
      //! \copydoc ConfigLocationBase::ConfigLocationBase
      ConfigLocation(std::string applicationName, std::string vendorName = "") :
        ConfigLocationBase(applicationName, vendorName) {};

      //! \copydoc IConfigLocation::get_config_location
      std::filesystem::path get_config_location() const override;
  };
}
