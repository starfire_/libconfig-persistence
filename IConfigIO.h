/*
 * IConfigIO.h
 *
 * Copyright 2025 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file IConfigIO.h
  * \brief Declaration of the interface of the ConfigIO class
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2025-01-20
  */

#pragma once

#include <string>
#include <map>
#include <memory>
#include <tinyxml2.h>


//! \namespace cfg_persistence
namespace cfg_persistence {
  /** \brief Interface for the Config IO class
    *
    * This interface defines the methods that need to be available in an
    * implementation of the ConfigIO class
    */
  class IConfigIO
  {
    public:
      //! \brief Virtual destructor
      virtual ~IConfigIO() {};

      /** \brief Set a file extenstion
        *
        * This method is used to set a file extension for the configuration
        * file. The standard file extension is \c .cfg
        *
        * \param file_ext File extension for configuration file
        */
      virtual void set_config_file_extension(std::string file_ext) = 0;

      /** \brief Check if configuration exists
        *
        * This method checks if a configuration file for the given
        * application with the given file extension already exists.
        *
        * \retval true A configuration file exists
        * \retval false No configuration file found
        */
      virtual bool config_exists() = 0;

      /** \brief Create configuration directory
        *
        * This method creates the actual configuration directory and all
        * directories to get to it if necessary
        */
      virtual void create_config_directory() = 0;

      /** \brief Read the config file as tag-value file
        *
        * This method expects the defined configuration file to be a so-
        * called tag-value file, i.e. <tt>key = value</tt>
        *
        * \return Map containing read key/value pairs
        *
        * \remark A map is always returned, but it may be empty
        * \remark It is the callers responsibility to delete the returned
        *         map
        */
      virtual std::map<std::string, std::string> read_config_tag_value() = 0;
      /** \brief Write the configuration as tag-value file
        *
        * This method expects a map conatining the key/value pairs which are
        * written to the configuration file as tag-value file.
        *
        * \param tag_value_data Key/Value pairs to be written
        *
        * \remark Any existing configuration file will be overwritten
        */
      virtual void save_config_tag_value(std::map<std::string, std::string> tag_value_data) = 0;

      /** \brief Read the config file as XML file
        *
        * This method expects the configuration file to be an XML file. It
        * uses the TinyXML2 library to load it.
        *
        * \return XML document as a pointer to TinyXML2 XMLDocument
        */
      virtual tinyxml2::XMLDocument *read_config_xml() = 0;
      /** \brief Write the configuration file as XML
        *
        * This method expects a pointer to a TinyXML2 XMLDocument and saves
        * it as a XML file.
        *
        * \param xml_config_data Pointer to TinyXML2 document containing the
        *                        configuration
        *
        * \remark Any existing configuration file will be overwritten
        */
      virtual void save_config_xml(tinyxml2::XMLDocument *xml_config_data) = 0;
  };

  /** \typedef std::shared_ptr<IConfigIO> IConfigIOPtr
    * \brief Simpler name for shared pointer
    */
  using IConfigIOPtr = std::shared_ptr<IConfigIO>;
}

