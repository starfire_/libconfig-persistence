/*
 * ConfigIO.h
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file ConfigIO.h
  * \brief Declaration of the ConfigIO class
  * \author Andreas Tscharner <stupidmail.ch>
  * \date 2023-03-12
  */


#include "IConfigIO.h"
#include "IConfigLocation.h"


//! \namespace cfg_persistence
namespace cfg_persistence {

  /** \brief Class for the actual IO of the configuration
    *
    * This class is responsible for the actual IO of the configuration, i.e.
    * it is responsible to create the configuration directory and provides
    * methods to read, parse and write the configuration files.
    */
  class ConfigIO : public IConfigIO
  {
    private:
      cfg_persistence::IConfigLocationPtr app_location;   //!< Internal location class
      std::string config_file_extension;                //!< File extension of config file

    public:
      /** \brief Constructor
        *
        * This constructor expects a valid ConfigLocation class. It takes
        * the necessary data from it
        *
        * \param app_location Instance of ConfigLocation class
        */
      ConfigIO(cfg_persistence::IConfigLocationPtr app_location);
      ConfigIO() = delete;

      //! \copydoc IConfigIO::set_config_file_extension
      void set_config_file_extension(std::string file_ext);

      //! \copydoc IConfigIO::config_exists
      bool config_exists();

      //! \copydoc IConfigIO::create_config_directory
      void create_config_directory();

      //! \copydoc IConfigIO::read_config_tag_value
      std::map<std::string, std::string> read_config_tag_value();
      //! \copydoc IConfigIO::save_config_tag_value
      void save_config_tag_value(std::map<std::string, std::string> tag_value_data);

      //! \copydoc IConfigIO::read_config_xml
      tinyxml2::XMLDocument *read_config_xml();
      //! \copydoc IConfigIO::save_config_xml
      void save_config_xml(tinyxml2::XMLDocument *xml_config_data);
  };
}
