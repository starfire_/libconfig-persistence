/*
 * IConfigLocation.h
 *
 * Copyright 2021-2025 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file IConfigLocation.h
  * \brief Declaration of the Interface of the ConfigLocation class
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2025-01-20
  */

#pragma once

#include <filesystem>

/** \namespace cfg_persistence
  * \brief Namespace for library
  *
  * Main namespace for the libconfig-persistence library
  */
namespace cfg_persistence {

  /** \brief Interface for the ConfigLocation class
    *
    * This interface defines the methods that need to be (at least) in any
    * ConfigLocation class implementation
    */
  class IConfigLocation
  {
    public:
      //! Virtual destructor
      virtual ~IConfigLocation() {};

      /** \brief Return the directory for the configuration
        *
        * This method returns the directory where the configuration should
        * be stored according to the operation system standard.
        *
        * \return Path object to config directory
        */
      virtual std::filesystem::path get_config_location() const = 0;
      /** \brief Return the filename for the configuration
        *
        * This method returns the full filepath to the filename for the
        * configuration according to the operation system standard. The file
        * extension is optional.
        *
        * \param file_ext File extension for config file, optional. ".cfg"
        *                 if ommitted
        *
        * \return File path object to config file
        */
      virtual std::filesystem::path get_config_file(std::string file_ext = ".cfg") const = 0;
      /** \brief Return the filename as string
        *
        * This method returns the full filepath to the filename for the
        * configuration as string.
        *
        * \param file_ext File extension for config file, optional. ".cfg"
        *                 if ommitted
        *
        * \return File path to configuration file as string
        *
        * \see get_config_file
        */
      virtual std::string get_config_filename(std::string file_ext = ".cfg") const = 0;
  };

  /** \typedef std::shared_ptr<IConfigLocation> IConfigLocationPtr
    * \brief Simpler name for shared pointer
    */
  using IConfigLocationPtr = std::shared_ptr<IConfigLocation>;
}
