/*
 * ConfigLocationBase.h
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */


/** \file ConfigLocationBase.h
  * \brief Declaration of the ConfigLocationBase class
  *
  * The base class of ConfigLocation implements all the common methods of
  * the IConfigLocation interface. All different methods (for different
  * platforms) are implemented in separate derived classes.
  *
  * \author Andreas Tscharner <andy@stupidmail.ch>
  * \date 2023-03-13
  */

#pragma once

#include "IConfigLocation.h"

#include <filesystem>

//! \namespace cfg_persistence
namespace cfg_persistence {
  /** \brief Class that handles config directories and files
    *
    * This class creates directory and file paths where the config should
    * be stored. It is the base class and handles all common methods for all
    * supported operation systems
    */
  class ConfigLocationBase : public IConfigLocation
  {
    protected:
      std::string appName;    //!< Name of the application
      std::string vendorName; //!< Name of the vendor of the application

      /** \brief Get environment variable
        *
        * This method returns the value of the given environment variable if
        * it exists or the empty string otherwise
        *
        * \param envVar Name of environment variable
        *
        * \retval value Value of environment variable if it exists
        * \retval "" if the environment variables does not exist
        */
      std::string get_env_var(std::string envVar) const;

    public:
      /** \brief Constructor
        *
        * This is the constructor for this class. It expects the name of the
        * application and of the vendor as a string parameters
        *
        * \param applicationName Name of the application
        * \param vendorName Name of the vendor
        */
      ConfigLocationBase(std::string applicationName, std::string vendorName = "");
      ConfigLocationBase() = delete;

      //! \copydoc IConfigLocation::get_config_file
      std::filesystem::path get_config_file(std::string file_ext = ".cfg") const override;
      //! \copydoc IConfigLocation::get_config_filename
      std::string get_config_filename(std::string file_ext = ".cfg") const override;
  };

}
