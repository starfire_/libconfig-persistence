/* SPDX-License-Identifier: Apache-2.0
 * ConfigLocation.macos.cxx
 *
 * Copyright 2023 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "ConfigLocation.h"

#include <cstdlib>


namespace cfg_persistence {
  std::filesystem::path ConfigLocation::get_config_location() const
  {
    constexpr auto USER_HOME = "HOME";


    std::string user_home = this->get_env_var(USER_HOME);

    if (user_home.empty())
      throw std::runtime_error("No suitable location found");

    return std::filesystem::path(user_home) / std::filesystem::path("Library/Application Support") / std::filesystem::path(this->appName);
  };
}
