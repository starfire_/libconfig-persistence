/* SPDX-License-Identifier: Apache-2.0
 * ConfigLocationBase.cxx
 *
 * Copyright 2021-2023 Andreas Tscharner <andy@stupidmail.ch>
 */


#include "ConfigLocation.h"

#include <cstdlib>


namespace cfg_persistence {
  std::string ConfigLocationBase::get_env_var(std::string envVar) const
  {
    char *val = std::getenv(envVar.c_str());
    if (val == nullptr)
      return "";
    else
      return val;
  };

  ConfigLocationBase::ConfigLocationBase(std::string applicationName, std::string vendorName)
  {
    this->appName = applicationName;
    this->vendorName = vendorName;
  };

  std::filesystem::path ConfigLocationBase::get_config_file(std::string file_ext) const
  {
    std::string filename = this->appName + file_ext;
    return this->get_config_location() / std::filesystem::path(filename);
  };

  std::string ConfigLocationBase::get_config_filename(std::string file_ext) const
  {
    return this->get_config_file(file_ext).string();
  };
}
